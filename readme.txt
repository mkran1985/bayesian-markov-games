How to Run
----------
./[binary] [horizon] [tau] [epsilon] [0 or 1 - for generating all or first equilibria] < [input file path] 2>&1 |tee [output file path]

Example
----------
make game_bin.cc 
g++ game_bin.cc -ogame
./game 1 0.5 0 0 < input.txt 2>&1 |tee output1.txt
