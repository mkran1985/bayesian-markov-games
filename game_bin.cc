#include "game.h"


int main(int argc, char* argv[]) {
	
  Environment env;
	env.hor_ = atoi(argv[1]);
	env.tau_ = atof(argv[2]);
	env.epsilon_ = atof(argv[3]);
	env.exit_ = atoi(argv[4]);
  env.ReadInput();
  env.PrintDebug();
  // Initialize the JointAgentPolicy to be an empty vector of size num agents.
  JointAgentPolicy jp(env.getNumAgents());
	clock_t begin = clock();
  env.GenerateAllEqulibriumPolicies(jp, 0, begin);
  cout<<"\nNumber of Equilibria Found = "<<env.noOfEqs_<<endl;
  return 0;
}
