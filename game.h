#include <vector>
#include <map>
#include <set>
#include <iostream>
#include <ctime>
//#include <cmath>

using namespace std;


const static float kDefaultReward = 0;
const static int MAX_HORIZONS = 10;
const static int noOfEqs = 0;

typedef int StateId;

// JoinState is a vector of integers corresponding to the location of each agent.
typedef vector<StateId> JointState;
typedef int JointStateId;

typedef int Horizon;

typedef pair<JointStateId, float> JointStateProbability;
typedef pair<StateId, float> StateProbability;

// An action is one of 'E', 'W', 'N', 'S', 'L' or '*'
// The '*' corresponds to 'any'.
typedef char Action;

// A joint action is the list of actions made by each agent.
// The vector will always be of size numAgents.
// Assuming 5 possible actions and 5 agents,
// JointAction could potentially take 5*5*5*5*5 = 3125 distinct values.
typedef vector<Action> JointAction;

// The type of a player is represented as an integer.
typedef int Type;
// The joint type is a combination of the types of the agents.
typedef vector<Type> JointType;

typedef int AgentId;

typedef pair<Type, Action> PolicyEntry;

typedef map<PolicyEntry, float>  Policy;

struct BestResponseEntry {
  Horizon h_;
  JointStateId jsi_;
  Type type_;
  BestResponseEntry(Horizon h, JointStateId jsi, Type type) :
    h_(h), jsi_(jsi), type_(type) {
  }
  bool operator== (const BestResponseEntry& a) const {
    return (h_ == a.h_) && (jsi_ == a.jsi_) && (type_ == a.type_);
  }
  bool operator< (const BestResponseEntry& a) const {
    return (h_ < a.h_) || ((h_ == a.h_) && (jsi_ < a.jsi_)) ||
           ((h_== a.h_) && (jsi_ == a.jsi_) && (type_ < a.type_));
  }
};

BestResponseEntry make_bre(Horizon h, JointStateId jsi, Type type) {
  BestResponseEntry bre(h, jsi, type);
  return bre;
}

typedef map<BestResponseEntry, float> BestResponseMap;

typedef map<JointStateId, map<JointAction,  vector<JointStateProbability> > > JointTransitions;
typedef map<StateId, map<JointAction, vector<StateProbability> > > Transitions;

class Environment;

class States {
 private:
  int numStates_;
  vector<StateId> allStates_;
  // Transitions corresponding to each agent.
  map<AgentId, Transitions> transitions_;

  // Total Number of States.
  // Assuming 5 agents, and 20 positions in the grid, there will be a total of
  // 20 * 19 * 18 * 17 * 16 = 2 M possible states.
  int numJointStates_;
  vector<JointState> allJointStates_;
  JointTransitions jointTransitions_;

 public:

  States(int numStates, int numJointStates);

  void addState(StateId s);
  void addTransition(const AgentId& id,
                     const StateId& startState,
                     const JointAction& jAction,
                     const StateId& endState,
                     float probability);

  void addJointState(JointState s);

  void addJointTransition(const JointStateId& startState,
                          const JointAction& jAction,
                          const JointStateId& finishState,
                          float probability);

  vector<JointStateProbability> getNextState(const JointStateId& startState,
                                        const JointAction& jAction);
  JointState getState(JointStateId s) {
    return allJointStates_[s];
  }

  int getNumJointStates() const {
    return numJointStates_;
  }

  void PrintJointState(JointStateId s) {
    cout << "[";
    for (int i = 0; i < allJointStates_[s].size(); ++i) {
      cout << allJointStates_[s][i] << " ";
    }
    cout << "]";
  }
};

class TypeReward {
  private:
  // Map storing the immediate rewards for a particular State / Joint action
  // combination.
  map<JointStateId, map<JointAction, float> > rewards_;

  vector<Action>* actionList_;

  public:

  void addReward(const JointStateId& startState,
                 const JointAction& jAction,
                 float reward);

  float getReward(const JointStateId& state,
                  const JointAction& jAction) {
      if (rewards_[state].find(jAction) == rewards_[state].end()) {
          return kDefaultReward;
      } else {
        return rewards_[state][jAction];
      }
  }

  TypeReward(vector<Action>* actionList) {
    actionList_ = actionList;
  }

  TypeReward(const TypeReward& tr) {
    rewards_ = tr.rewards_;
    actionList_ = tr.actionList_;
  }
};

typedef map<Horizon, Policy> AgentPolicy;

class Agent {
 private:
  // Unique id of a agent.
  AgentId id_;
  // Number of possible types of the player.
  // Each type is a number from 0 to types - 1;
  int numTypes_;
  // Type indices.
  vector<int> typeIndices_;

  Type type_;
  
  BestResponseMap brMap_;
  
  map<pair<Horizon, Type>, float> finalBr_;

  // Belief map hold the player's belief.
  // Key: id of the other player
  // Value: probability of the type of the other player. One float per type.
  map<AgentId, vector<float> > belief_[MAX_HORIZONS];
  map<Type, TypeReward*> rewards_;

  // All joint actions of the other agents.
  vector<JointAction> allJointActions_;



  AgentPolicy* policy_;

  void permuteActions(JointAction jAction, int p);
  void permutePolicy(int t, int a);

  Environment* env_; // Not owned.

 public:
  vector<AgentPolicy> allAgentPolicies_;
  Agent(AgentId id, int types, Environment* env) {
    id_ = id;
    numTypes_ = types;
    env_ = env;
    type_ = 0;
    allAgentPolicies_.clear();
  }

  bool CheckPolicy();
  bool EvaluateOtherPolicy();

  void setFinalBrMap(Horizon h, Type type, float score) {
    finalBr_[make_pair(h, type)] = score;
  }

  float getFinalBrMap(Horizon h, Type type) {
    return finalBr_[make_pair(h, type)];
  }
  
  bool getBestResponseScore(Horizon h, JointStateId jsi, Type type, float* score)  const {
    if (brMap_.find(make_bre(h, jsi, type)) != brMap_.end()) {
      *score = brMap_.find(make_bre(h, jsi, type))->second;
      return true;
    } else {
      return false;
    }
  }

  void clearBrMap() {
    brMap_.clear();
  }

  Action getSampledAction(Horizon h, AgentPolicy* agentPol) const;

  void PrintPolicy();
  void PrintBelief();
  
  void generateAllJointActions();
  void permutePolicies(AgentPolicy policy, 
                       const vector<pair<Horizon, Type> >& allJointStateTypeCombos,
                       int c);
  void generateAllPolicies();

  float getActionProbability(Horizon h, Type t, Action a) const {
    if (policy_->find(h) != policy_->end() &&
        policy_->find(h)->second.find(make_pair(t, a))  !=
        policy_->find(h)->second.end()) {
      return 
        policy_->find(h)->second.find(make_pair(t, a))->second;
    } else {
      return 0;
    }
  }

  float computeJointActionProbability(Horizon h, const JointAction& jActions) const;

  void getActionUtility(Horizon h, JointStateId jsi, Type t, float* scores);

  float getImmediateReward(const JointStateId& state,
                           const JointAction& jAction,
                           Type type) const;

  AgentId getId() const {
      return id_;
  }

  vector<AgentPolicy>* getAllAgentPolicies() {
    return &allAgentPolicies_;
  }

  int getNumPolicies() {
    return allAgentPolicies_.size();
  }

  AgentPolicy* getAgentPolicy(int i) {
    return &allAgentPolicies_[i];
  }

  void setPolicy(AgentPolicy* policy) {
    policy_ = policy;
  }

  int getNumTypes() const {
    return numTypes_;
  }

  int getType() const {
    return type_;
  }

  int getTypeIndex(int t) const {
    return typeIndices_.at(t);
  }
  
  void addTypeIndex(int tIndex) {
    typeIndices_.push_back(tIndex);
  }
  
  void addBelief(Horizon h, AgentId id, const vector<float>& b) {
    belief_[h-1][id] = b;
  }

  float getBelief(Horizon h, AgentId id, Type t) const {
    if (belief_[h-1].find(id) != belief_[h-1].end()) {
      return belief_[h-1].find(id)->second.at(t);
    } else {
      return 0;
    }
  }

  void addTypeReward(Type type, TypeReward* typeReward) {
    rewards_[type] = typeReward;
  }
};

typedef vector<AgentPolicy*> JointAgentPolicy; 

class Environment {
 private:
  States* space_;
  int numAgents_;
  float discount_;


	
  // Map of all players keyed by their index.
  map<int, Agent*> agents_;
  map<AgentId, int> agentIndexMap_;
  vector<Action> actionList_;
  vector<TypeReward*> typeRewardList_;
  vector<JointAction> allJointActions_;

  vector<map<Action, float> > allActionDistributions_;
  JointStateId start_[MAX_HORIZONS];
  vector<JointAgentPolicy> equilibriumPolicies_;

 void permuteDistributions(map<Action, float> actionDistribution, int a, float total);
 void generateAllActionDistributions();

 public:
 int noOfEqs_;
 int hor_;
 float tau_;
 float epsilon_;
 int exit_;

  ~Environment () {
    for (int i = 0; i < typeRewardList_.size(); ++i) {
      delete typeRewardList_[i];
    }
    for (int i = 0; i < numAgents_; ++i) {
      delete agents_[i];
    }
  }

  void SamplePolicies(JointAgentPolicy& policies);
  bool TestEquilibriumOfJointPolicy(JointAgentPolicy& policies, clock_t begin);
  void GenerateAllEqulibriumPolicies(JointAgentPolicy jp, int p, clock_t begin);
  
  void ReadInput();

  void PrintPolicies() {
    for (int i = 0; i < numAgents_; ++i) {
      getMutableAgentByIndex(i)->PrintPolicy();
    }
  }

	vector<map<Action, float> >* getAllActionDistributions() {
    return &allActionDistributions_;
  }

  float getEpsilon() const {
    return epsilon_;
  }

  float getTau() const {
    return tau_;
  }

  JointStateId getStartState(Horizon h) {
    return start_[h-1];
  }

  JointStateId setStartState(Horizon h, JointStateId jsi) {
    start_[h-1] = jsi;
  }

  int getActionListSize() {
    return actionList_.size();
  }

  int getNumAgents() const {
     return numAgents_;
  }

  float getDiscount() const {
      return discount_;
  }

  AgentId getAgentId(int k) {
      return agents_[k]->getId();
  }

  int getAgentIndex(AgentId id) {
      return agentIndexMap_[id];
  }

  Action getAction(int i) {
      return actionList_[i];
  }

  const Agent* getAgentByIndex(int i) {
    return agents_[i];
  }
  
  Agent* getMutableAgentByIndex(int i) {
    return agents_[i];
  }

  const Agent* getAgentById(AgentId id) {
    return agents_[agentIndexMap_[id]];
  }

  vector<JointStateProbability> getNextState(const JointStateId& startState,
                                        const JointAction& jAction) {
      return space_->getNextState(startState, jAction);
  }

  const States* getSpace() {
    return space_;
  }

  void PrintDebug() {

//    cout <<"---------------------------------"<<endl;
    cout <<"Domain Summary"<<endl;
    cout <<"---------------------------------"<<endl;
    cout << "Num Agents: \t\t" << numAgents_ << endl;
    cout << "Num Horizons: \t\t" << hor_ << endl;
    cout << "Discount: \t\t\t" << discount_ << endl;
    cout << "Tau: \t\t\t\t" << tau_ << endl;
    cout << "Epsilon: \t\t\t" << epsilon_ << endl;
    cout << "Num Joint States: \t" << space_->getNumJointStates() << endl;
    cout << "Start State: \t\t"<<getStartState(hor_)<<endl;
    cout << "Action List: \t\t";
    for (int a = 0; a < actionList_.size(); ++a) {
      cout << getAction(a) << " ";
    }
    cout << endl;
    cout << "Total Types: \t\t" << typeRewardList_.size() << endl;
    unsigned int numJType= 1;
    vector<int> numAgTypes(getNumAgents(),0);
    for (int ag=0; ag<getNumAgents(); ag++){
		const Agent* agt = getAgentByIndex(ag);
		numAgTypes[ag] = agt->getNumTypes();
		numJType *= numAgTypes[ag];
	}
    cout << "Num Joint Types: \t"<< numJType << endl;
    cout << "Num Joint Actions: \t" << allJointActions_.size() << endl;


//    cout << "Rewards: " << endl;
//    for (int i = 0; i < typeRewardList_.size(); ++i) {
//      cout << "Type : " << i << endl;
//      for (JointStateId s = 0; s < space_->getNumJointStates(); ++s) {
//        space_->PrintJointState(s);
//        cout << " : ";
//        for (int j = 0; j < allJointActions_.size(); ++j) {
//          cout << typeRewardList_[i]->getReward(s, allJointActions_[j]) << "  ";
//        }
//        cout << endl << endl;
//      }
//    }

    for (int ag=0; ag<getNumAgents(); ag++){
		const Agent* agt = getAgentByIndex(ag);
		cout <<"-------------"<<endl;
		cout <<"Agent "<< ag <<endl;
		cout <<"-------------"<<endl;
//		cout <<"No. of others' joint actions :" << allJointActions_.size() << endl;
		cout <<"Total H x T: \t\t"<< hor_ * agt->getNumTypes() << endl;
		cout <<"Total Policies: \t" << agt->allAgentPolicies_.size() << endl;
	}
    cout <<"---------------------------------"<<endl;

  }
};



States::States(int numStates, int numJointStates) {
  numStates_ = numStates;
  numJointStates_ = numJointStates;
}

void States::addJointState(JointState s) {
  allJointStates_.push_back(s);
}

void States::addJointTransition(const JointStateId& startState,
                           const JointAction& jAction,
                           const JointStateId& finishState,
                           float probability) {
  jointTransitions_[startState][jAction].push_back(
      make_pair(finishState, probability));
}

void States::addState(StateId s) {
  allStates_.push_back(s);
}

void States::addTransition(const AgentId& id,
                           const StateId& startState,
                           const JointAction& jAction,
                           const StateId& endState,
                           float probability) {
  transitions_[id][startState][jAction].push_back(make_pair(endState, probability));
}

vector<JointStateProbability> States::getNextState(const JointStateId& startState,
                                                   const JointAction& jAction) {
  if (jointTransitions_.find(startState) == jointTransitions_.end()) {
    cout << "Start State Missing. Exiting.";
    exit(0);
  }
  if (jointTransitions_[startState].find(jAction) == jointTransitions_[startState].end()) {
    cout << "Joint action missing. Exiting.";
    exit(0);
  }
  return jointTransitions_[startState][jAction];
}
  
Action Agent::getSampledAction(Horizon h, AgentPolicy* agentPol) const {
  vector<float> total;
  float sum = 0;
  for (int a = 0; a < env_->getActionListSize(); a++) {
    sum += (*agentPol)[h][make_pair(getType(), env_->getAction(a))];
    total.push_back(sum*100);
  }
  int v = rand() % 100;
  for (int a = 0; a < total.size(); ++a) {
    if (v <= total[a]) {
      return env_->getAction(a);
    }
  }
  return env_->getAction(env_->getActionListSize() - 1);
}

void Agent::PrintPolicy() {
  cout << endl;
//  cout << "---------------------------------" << endl;
  cout << "Agent: " << id_ << endl;
  cout << "---------------------------------" << endl;
//  cout << "Number of Types: " << getNumTypes() <<endl;
  for (Horizon h = 1; h <= env_->hor_; ++h) {
	cout << "Horizon: " << h << " Score: " << getFinalBrMap(h, getType()) << endl;
	cout <<"-------------"<<endl;
	cout <<"State: "<<env_->getStartState(h)<<endl;
	cout <<"-------------"<<endl;
	for (int ty = 0; ty < getNumTypes(); ty++){
    	cout << "Type: " << typeIndices_[ty] <<endl;
		for (int a = 0; a < env_->getActionListSize(); ++a) {
				//cout<<"blah :"<<a<<endl;
//		  cout << "A: " << env_->getAction(a) << " : "
//			<< (*policy_)[h][make_pair(type_, env_->getAction(a))];
			cout << "Action: " << env_->getAction(a) << " : "
						<< (*policy_)[h][make_pair(ty, env_->getAction(a))];
			cout << endl;
		}
		cout <<"-------------"<<endl;
    }
    cout << "Beliefs: "<<endl;
    for (int j = 0; j < env_->getNumAgents(); ++j) {
		if (id_ == j) {
		  continue;
		}
		const Agent* agentj = env_->getAgentByIndex(j);
		vector<float> belief(agentj->getNumTypes());
		for (Type k = 0; k < agentj->getNumTypes(); ++k) {
		  cout<<getBelief(h, agentj->getId(), k)<<" ";
		}
		cout<<endl;
	}

    cout << "---------------------------------" << endl;
  }
  //cout << "---------------------------------" << endl;
}

void Agent::PrintBelief(){
//	cout << "---------------------------------" << endl;
  cout << "Final Beliefs of Agent: "<<id_<<endl;
	cout << "---------------------------------" << endl;
	for (Horizon t = 1; t <= env_->hor_; ++t) {
		cout << "Horizon: " << t << endl;
		cout << "---------------" << endl;
  	for (int j = 0; j < env_->getNumAgents(); ++j) {
        if (id_ == j) {
          continue;
        }
        const Agent* agentj = env_->getAgentByIndex(j);
        vector<float> belief(agentj->getNumTypes());
        for (Type k = 0; k < agentj->getNumTypes(); ++k) {
          cout<<" "<<getBelief(t, agentj->getId(), k);
        }
				cout<<endl;
    }
	cout << "---------------------------------" << endl;
	}
}
  
void Agent::permuteActions(JointAction jAction, int p) {
  if (p == env_->getNumAgents()) {
    allJointActions_.push_back(jAction);
    return;
  } else if (env_->getAgentId(p) == id_) {
    jAction[p] = '*';
    permuteActions(jAction, p+1);
  } else {
    const Agent* agent = env_->getAgentByIndex(p);
    for (int i = 0; i < env_->getActionListSize(); ++i) {
      jAction[p] = env_->getAction(i);
      permuteActions(jAction, p+1);
    }
  }
}

void Agent::generateAllJointActions() {
  JointAction jAction;
  jAction.resize(env_->getNumAgents());
  permuteActions(jAction, 0);
//  cout <<"-------------"<<endl;
//  cout <<"Agent "<< id_ <<endl;
//  cout <<"-------------"<<endl;
//  cout<<"No. of others' joint actions :" << allJointActions_.size() << endl;
}

void Agent::permutePolicies(AgentPolicy policy, 
                            const vector<pair<Horizon, Type> >& allHorizonTypeCombos,
                            int c) {
  if (c == allHorizonTypeCombos.size()) {
    allAgentPolicies_.push_back(policy);
    return;
  }
  const pair<Horizon, Type>& ht = allHorizonTypeCombos.at(c);
  // Populate all policies.
  vector<map<Action, float> >* allActionDistributions = env_->getAllActionDistributions();
  for (int i = 0; i < allActionDistributions->size(); ++i) {
    map<Action, float>& actionDistribution = (*allActionDistributions)[i];
    for (int a = 0; a < env_->getActionListSize(); ++a) {
      PolicyEntry pe(ht.second, env_->getAction(a));
      policy[ht.first][pe] = actionDistribution[env_->getAction(a)];
    }
    permutePolicies(policy, allHorizonTypeCombos, c+1);
  }
}

void Agent::generateAllPolicies() {
  vector<pair<Horizon, Type> > allHorizonTypeCombos;
  for (int h = 1; h <= env_->hor_; ++h) {
    for (int t = 0; t < getNumTypes(); ++t) {
      allHorizonTypeCombos.push_back(make_pair(h, t));
    }
  }
//  cout <<"Total H x T: \t\t"<< allHorizonTypeCombos.size() << endl;
  AgentPolicy policy;
  permutePolicies(policy, allHorizonTypeCombos, 0);
//  cout <<"Total Policies: \t" << allAgentPolicies_.size() << endl;
}
 
// Expand all the '*'
void getAllJointActions(const JointAction& jAction,
                        const vector<Action>* actionList,
                        vector<JointAction>* jActions) {
  bool star = false;
  for (int i = 0; i < jAction.size(); i++) {
    // Replace the first '*' with all possible values of Action and then
    // recurse.
    if (jAction.at(i) == '*') {
      star = true;
      for (int j = 0; j < actionList->size(); ++j) {
        JointAction action = jAction;
        action[i] = actionList->at(j);
        getAllJointActions(action, actionList, jActions);
      }
    }
  }
  if (star == false) {
    jActions->push_back(jAction);
  }
  return;
}

void TypeReward::addReward(const JointStateId& startState,
                           const JointAction& jAction,
                           float reward) {
  vector<JointAction> allJointActions;
  getAllJointActions(jAction, actionList_, &allJointActions);
  for (int i = 0; i < allJointActions.size(); ++i) {
    rewards_[startState][allJointActions[i]] = reward;
  }
}

float Agent::getImmediateReward(const JointStateId& state,
                                const JointAction& jAction,
                                Type type) const {
    if (rewards_.find(type) != rewards_.end()) {
      return rewards_.find(type)->second->getReward(state, jAction);
    } else {
      return 0;
    }
}

float Agent::computeJointActionProbability(Horizon h,
                                           const JointAction& jAction) const {
  float jActionProbability = 1;
  for (int i = 0; i < env_->getNumAgents(); ++i) {
    // Iterate only through the types of other agents.
    if (env_->getAgentId(i) == id_) {
      continue;
    }
    float actionProbability = 0;
    const Agent* agent = env_->getAgentByIndex(i);
    for (Type t = 0; t < agent->getNumTypes(); ++t) {
      actionProbability +=
        getBelief(h, agent->getId(), t) * agent->getActionProbability(h, t, jAction[i]);
    }
    jActionProbability *= actionProbability;
    if (jActionProbability == 0) {
      break;
    }
  }
  return jActionProbability;
}

// Get the action utility for the given h, joint state, and action and type.
void Agent::getActionUtility(Horizon h,
                             JointStateId jsi,
                             Type t,
                             float* score) {
  if (h == 0) {
    *score = 0;
    return;
  }
  if (getBestResponseScore(h, jsi, t, score)) {
    return;
  }

  for (int i = 0; i < allJointActions_.size(); ++i) {
    // For other agents.
    float jointActionProbability = computeJointActionProbability(h, allJointActions_[i]);
    // For all the actions of this agent.
    float actionValue = 0;
    JointAction jAction = allJointActions_[i];
    for (int j = 0; j < env_->getActionListSize(); ++j) {
        // Set the action of this agent.
        jAction[env_->getAgentIndex(id_)] = env_->getAction(j);
        float immediateReward = getImmediateReward(jsi, jAction, t);
        // Compute future rewards based on the transition probabiities.
        float futureRewards = 0;
        vector<JointStateProbability> nextState =
            env_->getNextState(jsi, jAction);
        for (int s = 0; s < nextState.size(); ++s) {
          const JointStateId& x = nextState[s].first;
          float stateProbability = nextState[s].second;
          float scr = 0;
          getActionUtility(h - 1, x, t, &scr);
          futureRewards += stateProbability * scr;
        }
        futureRewards *= env_->getDiscount();
        actionValue += getActionProbability(h, t, env_->getAction(j)) * (immediateReward + futureRewards);
     }
    *score += jointActionProbability * actionValue;
  }
  brMap_[make_bre(h, jsi, t)] = *score;
  return;
}

bool Agent::CheckPolicy() {
  // Compute the scores for the true type for all horizons.
  for (Horizon h = 1; h <= env_->hor_; ++h) {
    clearBrMap();
    float score = 0;
    getActionUtility(h, env_->getStartState(h), getType(), &score);
    setFinalBrMap(h, getType(), score);
  }
  // Iterate through all the policies of this agents to check if the chosen policy is the best response policy.
  vector<AgentPolicy>* allAgentPolicies = getAllAgentPolicies();
  for (int i = 0; i < getNumPolicies(); ++i) {
    setPolicy(getAgentPolicy(i));
    if (!EvaluateOtherPolicy()) {
      return false;
    }
  }
  return true;
}

bool Agent::EvaluateOtherPolicy() {
  // A policy has been chosen for each horizon. Check for equilibrium.
  for (Horizon h = 1; h <= env_->hor_; ++h) {
    clearBrMap();
    float score = 0;
    getActionUtility(h, env_->getStartState(h), getType(), &score);
    if (score > getFinalBrMap(h, getType()) + env_->getEpsilon()) {
      // Found a policy where the score is greater than the score of the chosen policy.
      return false;
    }
  }
  return true;
}

void Environment::SamplePolicies(JointAgentPolicy& policies) {
  for (Horizon t = hor_; t > 1; --t) {
    // Sample joint actions
    JointAction sampledJActions(getNumAgents());
    for (int i = 0; i < getNumAgents(); ++i) {
      const Agent* agent = getAgentByIndex(i);
      sampledJActions[i] = agent->getSampledAction(t, policies[i]);
    }

    JointStateId s = getStartState(t); 
    //sample next state
    JointStateId nextJState = space_->getNumJointStates() - 1;
    vector<JointStateProbability> nextState = getNextState(s, sampledJActions);
    vector<float> total;
    float sum = 0;
    for (int i = 0; i < nextState.size(); ++i) {
      sum += nextState[i].second;
      total.push_back(sum * 100);
    }
    int v = rand() % 100;
    for (int i = 0; i < total.size(); ++i) {
      if (v <= total[i]) {
        nextJState = nextState[i].first;
        break;
      }
    }
    setStartState(t - 1, nextJState);

    //belief update
    for (int i = 0; i < getNumAgents(); ++i) {
      Agent* agenti = getMutableAgentByIndex(i);
      for (int j = 0; j < getNumAgents(); ++j) {
        if (i == j) {
          continue;
        }
        const Agent* agentj = getAgentByIndex(j);
        vector<float> belief(agentj->getNumTypes());
        float total = 0.00000000000001;
        for (Type k = 0; k < agentj->getNumTypes(); ++k) {
          belief[k] = agenti->getBelief(t, agentj->getId(), k) * (*policies[j])[t][make_pair(k, sampledJActions.at(j))];
          total += belief[k];
        }
        for (int k = 0; k < belief.size(); ++k) {
          belief[k] /= total;
        }
        // Update belief of the next horizon.
        agenti->addBelief(t - 1, agentj->getId(), belief);
      }
    }
  }
}

void Environment::GenerateAllEqulibriumPolicies(JointAgentPolicy jp, int p, clock_t begin) {
  if (p == getNumAgents()) {
    // We've picked a policy for each agent. Check if its an equilibrium policy.

    if (TestEquilibriumOfJointPolicy(jp, begin)) {
      equilibriumPolicies_.push_back(jp);

    }
    return;
  }
  Agent* agent = getMutableAgentByIndex(p);
  for (int i = 0; i < agent->getNumPolicies(); ++i) {
    jp[p] = agent->getAgentPolicy(i);
    GenerateAllEqulibriumPolicies(jp, p + 1, begin);
  }
  return;
}

bool  Environment::TestEquilibriumOfJointPolicy(JointAgentPolicy& policies, clock_t begin) {
  // Assigns beliefs for each agent, for each horizon.
  // Assigns joint states for each horizon.
  SamplePolicies(policies);
  for (int i = 0; i < getNumAgents(); ++i) {
    // Set all the agent's policies to the picked policy.
    for (int j = 0; j < getNumAgents(); ++j) {
      Agent* agent = getMutableAgentByIndex(j);
      agent->setPolicy(policies.at(j));
    }
    Agent* agent = getMutableAgentByIndex(i);
    if (!agent->CheckPolicy()) {
      return false;
    }
  }
  noOfEqs_++;
  cout << "\n*********************************";
  cout << "\nFound Equilibrium Policy #"<<noOfEqs_<<endl;
  cout << "*********************************";
  for (int i = 0; i < getNumAgents(); ++i) {
    Agent* agent = getMutableAgentByIndex(i);
		agent->setPolicy(policies.at(i));
		agent->PrintPolicy();
//		agent->PrintBelief();
  }
//  cout << "\n";
	clock_t end = clock();
	
	double elapsed_secs = double(end - begin) * 1000 / CLOCKS_PER_SEC;
	cout<<"Elapsed Time (millisec): "<<elapsed_secs<<endl;
	cout<<"Elapsed Time (sec): \t"<<elapsed_secs/1000<<endl;
	cout<<"Elapsed Time (min): \t"<<elapsed_secs/60000<<endl;
	if (exit_ != 0 || noOfEqs_ >= 5000 || elapsed_secs >= 3600000){
		cout<<"\nNumber of Equilibria Found >= "<<noOfEqs_<<endl;
		exit(0);
	}
  return true;
}

void Environment::permuteDistributions(map<Action, float> actionDistribution, int a, float total) {
  if (a == getActionListSize() - 1) {
    actionDistribution[getAction(a)] = 1 - total;
//    for (int i = 0; i < getActionListSize(); ++i) {
//      cout << getAction(i) << ": " << actionDistribution[getAction(i)] << ", ";
//    }
//    cout << "\n";
    allActionDistributions_.push_back(actionDistribution);
  } else {
    float  t = 1 - total;
    float p = 0;
    for (p = 0; p <= t; p += tau_) {
      actionDistribution[getAction(a)] = p;
      permuteDistributions(actionDistribution, a+1, total+p);
    }
    if (total + p < 1) {
      actionDistribution[getAction(a)] = 1 - total - p;
      permuteDistributions(actionDistribution, a+1, 1);
    }
  }
}

void Environment::generateAllActionDistributions() {
  map<Action, float> actionDistribution;
  permuteDistributions(actionDistribution, 0, 0);
}

int ipow(int base, int exp)
{
    int result = 1;
    while (exp)
    {
        if (exp & 1)
            result *= base;
        exp >>= 1;
        base *= base;
    }

    return result;
}
    
void Environment::ReadInput() {
	noOfEqs_ = 0;
  cin >> numAgents_;
  cin >> discount_;
//  cin >> tau_;
//  cin >> epsilon_;
  int S; // Number of states.
  int JS; // Number of joint states.
  cin >> S >> JS;
  space_ = new States(S, JS);
  for (int i = 0; i < JS; ++i) {
      JointState s;
      int x;
      for (int j = 0; j < numAgents_; ++j) {
          cin >> x;
          s.push_back(x);
      }
      space_->addJointState(s);
  }
  
  int A; // Number of actions.
  cin >> A;
  for (int i = 0; i < A; i++) {
     Action a;
     cin >> a;
     actionList_.push_back(a);
  }

//  cout << "All Distributions: " << endl;
  generateAllActionDistributions();
//  cout << "-------------------" << endl;

  JointStateId start; // Start state.
  cin >> start;
	//cout<<"--Start--:"<<start<<endl;
  setStartState(hor_, start);
  
  int T; // Total types.
  cin >> T;
//cout<<"--Types--:"<<T<<endl;
  for (int i = 0; i < numAgents_; ++i) {
      int types;
      cin >> types;
      Agent* agent = new Agent(i, types, this);
      for (int t = 0; t < types; ++t) {
          int typeIndex;
          cin >> typeIndex;
          agent->addTypeIndex(typeIndex);
      }
      agents_[i] = agent;
      agentIndexMap_[i] = i;
  }
  
  for (int i = 0; i < numAgents_; ++i) {
    agents_[i]->generateAllJointActions();
    agents_[i]->generateAllPolicies();
  }

  for (int i = 0; i < numAgents_; ++i) {
      for (int j = 0; j < numAgents_ - 1; ++j) {
        AgentId id;
        cin >> id;
//cout<<"--AgentId--:"<<id<<endl;
        vector<float> belief(getAgentById(id)->getNumTypes());
        for (int k = 0; k < belief.size(); ++k) {
            cin >> belief[k];
//cout<<"--belief--:"<<belief[k]<<endl;
        }
        for (int h = 1; h <= hor_; ++h) {
          agents_[i]->addBelief(h, id, belief);
        }
      }
  }

  int J;  // Total joint actions;
  cin >> J;
	//J = ipow(A,numAgents_);

  for (int i = 0; i < J; ++i) {
      JointAction jAction;
      for (int j = 0; j < numAgents_; ++j) {
          Action a;
          cin >> a;
//cout<<"--Action--:"<<a<<endl;
          jAction.push_back(a);
      }
      allJointActions_.push_back(jAction);
      for (JointStateId sid = 0; sid < JS; sid++) {
          for (JointStateId eid = 0; eid < JS; eid++) {
              float prob;
              cin >> prob;
              if (prob > 0) {
                  space_->addJointTransition(sid, jAction, eid, prob);
              }
          }
      }
  }
//cout<<"Done :"<<J<<endl;

  for (int i = 0; i < T; ++i) {
    TypeReward* typeReward = new TypeReward(&actionList_);
    for (JointStateId s = 0; s < JS; s++) {
      float reward;
      for (int j = 0; j < J; ++j) {
        cin >> reward;
				//cout<<" "<<reward;
        typeReward->addReward(s, allJointActions_[j], reward);
      }
			//cout<<endl;
    }
    typeRewardList_.push_back(typeReward);
  }
//cout<<"Done2"<<endl;

  for (int i = 0; i < numAgents_; ++i) {
    for (int j = 0; j < getAgentByIndex(i)->getNumTypes(); ++j) {
      int typeIndex = getAgentByIndex(i)->getTypeIndex(j);
      agents_[i]->addTypeReward(j, typeRewardList_[typeIndex]);
    }
  }
//cout<<"Done3"<<endl;
}
